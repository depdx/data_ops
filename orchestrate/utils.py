import glob
import os
from datetime import date, datetime, timedelta
from pathlib import Path
from shutil import copy
import pandas as pd

def get_scrape_ranges(start, end, span_days, date_format):

    IN_DATE_FORMAT = date_format
    OUT_DATE_FORMAT= '%m/%d/%Y'
    DATE_STEP = timedelta(days=1)

    start = datetime.strptime(start, IN_DATE_FORMAT)
    end   = datetime.strptime(end, IN_DATE_FORMAT)
    span  = timedelta(days=span_days)
    stop = end - span

    while start < stop:
        current = start + span
        yield date.strftime(start, OUT_DATE_FORMAT), date.strftime(current, OUT_DATE_FORMAT)
        start = current + DATE_STEP

    yield date.strftime(start, OUT_DATE_FORMAT), date.strftime(end, OUT_DATE_FORMAT)


class FileManagement:
    def __init__(self):

        self.objects = [
            "CaseOverviewItem",
            "CasePartyItem",
            "EventItem",
            "FileItem",
            "JudgmentItem",
            "LawyerItem",
        ]

    def copy_data_files(
        self,
        source_dir: str,
        destination_dir: str,
        starting_date: str,
        ending_date: str,
    ):

        file_date = f"{starting_date}__to__{ending_date}".replace("/", "_")
        print(file_date)

        for object in self.objects:
            object_dir = f"{destination_dir}/{object}"
            Path(object_dir).mkdir(parents=True, exist_ok=True)
            source_filename = f"{source_dir}/{object}.csv"
            destination_filename = f"{object_dir}/{file_date}.csv"
            copy(source_filename, destination_filename)

    def compile_data_files(self, scrape_path):

        for object in self.objects:
            target_path = f"{scrape_path}/{object}"
            compiled_target_path = f"{target_path}.csv"
            csv_path_string = f"{target_path}/*.csv"
            path = os.getcwd()
            csv_files = glob.glob(os.path.join(path, csv_path_string))

            c_df = pd.read_csv(
                compiled_target_path, index_col=None, header=0, encoding="ISO-8859-1"
            )

            li = []

            for filename in csv_files:
                if os.path.getsize(filename) > 0:
                    df = pd.read_csv(
                        filename, index_col=None, header=0, encoding="ISO-8859-1"
                    )
                    li.append(df)
                    
            if len(li) > 0:
                df = pd.concat(li, axis=0, ignore_index=True)
                df = df.replace({r'[^\x00-\x7F]+':''}, regex=True) # this might be too hasty
                df = pd.concat([c_df, df]).drop_duplicates().reset_index(drop=True)

                df.to_csv(compiled_target_path, index=False, index_label=False)
