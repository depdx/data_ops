import shutil
import os
import glob
import calendar
import pandas as pd
from datetime import datetime, date
from tqdm import tqdm
from itertools import islice


def copy_filings(destination: str, months_back: int) -> list:
    """
    copies pdf files from scraped filings to the designated destination and number of passed months
    returns a list of files moved
    """

    today = datetime.now()
    current_month = today.month
    end_month = today.month + 1
    start_month = current_month - months_back

    months = list(range(start_month, end_month, 1))

    file_names = []

    for month in months:
        month_n = "%02d" % (month,)
        month_name = calendar.month_name[month]
        pdf_path_string = f"/content/drive/Shareddrives/DAILY_SCRAPE_OJD/Daily_Cases_{month_n}_*_202*/**/22LT*.pdf"
        path = os.getcwd()
        pdf_files = glob.glob(os.path.join(path, pdf_path_string))

        print(f"copying files for {month_name}...")

        for filename in tqdm(pdf_files):
            shutil.copy(filename, f"/content/drive/MyDrive/Don't Evict PDX/Hub/{destination}")
            file_names.append(filename)

    return file_names


def get_upcoming_cases( updated_appearancesDF: pd.DataFrame) -> list:
    """
    takes an updated dataframe for court appearances, filters to upcoming appearances
    returns a list of case_codes for upcoming appearances
    """
    today = str(date.today())
    print("checking upcoming cases...")
    updated_appearancesDF["date"] = pd.to_datetime(updated_appearancesDF["date"])
    upcoming_appearancesDF = updated_appearancesDF[
        (updated_appearancesDF["date"] > today)
    ]
    case_codes = upcoming_appearancesDF["case_code"].to_list()
    case_count = len(case_codes)
    print(f"found {case_count} upcoming cases")

    return case_codes


def get_upcoming_files(
    case_codes: list,
    scrapes_drive_id: str,
    depdx_eviction_documents_id: str,
    gdrive,
) -> dict:
    """
    checks Drive for files matching the case codes, if unavailable it searches and gets
    them from OiC shared Drive and copies them to depdx's Drive
    """

    # Drive Env
    path = os.getcwd()

    pdf_files_needed = []
    cases_with_local_files = []
    cases_without_local_files = []
    cases_with_shared_drive_files = []
    cases_without_files = []

    print("checking local and shared drive for case files...")

    for case_code in tqdm(case_codes):

        pdf_path_string = f"/content/drive/MyDrive/Don't Evict PDX/Hub/Evictions/Documents/{case_code}*.pdf"
        local_pdf_files = glob.glob(os.path.join(path, pdf_path_string))

        if len(local_pdf_files) > 0:
            cases_with_local_files.append(case_code)

        else:
            cases_without_local_files.append(case_code)

            drive_files = gdrive.ListFile(
                {
                    "q": f"title contains '{case_code}'",
                    "driveId": scrapes_drive_id,
                    "corpora": "drive",
                    "includeItemsFromAllDrives": True,
                    "supportsAllDrives": True,
                }
            ).GetList()
            shared_drive_files = len(drive_files)

            if len(drive_files) > 0:
                cases_with_shared_drive_files.append(case_code)
            else:
                cases_without_files.append(case_code)

            stop = 2

            for file in islice(drive_files, 1, stop):
                pdf_files_needed.append(file["id"])

    number_pdf_files_needed = len(pdf_files_needed)

    print(f"copying {number_pdf_files_needed} files from shared Drive")

    for id in tqdm(pdf_files_needed):
        gdrive.auth.service.files().copy(
            fileId=id,
            supportsAllDrives=True,
            body={"parents": [{"id": depdx_eviction_documents_id}]},
        ).execute()

    print("\n")
    print(f"cases with local files {len(cases_with_local_files)}")
    print(f"cases without local files {len(cases_without_local_files)}")
    print(f"cases with shared drive files {len(cases_with_shared_drive_files)}")
    print(f"cases without files {len(cases_without_files)}")

    results = {
        "cases_with_local_files": cases_with_local_files,
        "cases_without_local_files": cases_without_local_files,
        "cases_with_shared_drive_files": cases_with_shared_drive_files,
        "cases_without_files": cases_without_files,
    }
