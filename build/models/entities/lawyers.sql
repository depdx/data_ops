WITH dedupe AS (

  SELECT

    * , 
    ROW_NUMBER() OVER(
      PARTITION BY case_code, name, party_name, 
                   status, striked
      ORDER BY _scrapy_timestamp DESC
    )

  FROM public.raw__lawyers

)

SELECT

  case_code,
  name,
  party_name,
  status,
  striked

FROM dedupe
WHERE row_number = 1