SELECT

  addr                                AS address,
  bldg_addr                           AS building_address,
  "OccupancyIdentifier"               AS unit,
  "AddressNumber"                     AS building_number,
  UPPER("StreetNamePreDirectional")   AS directional,
  INITCAP("StreetName")               AS street,
  INITCAP("StreetNamePostType")       AS steet_type,
  INITCAP("PlaceName")                AS city,
  "ZipCode"                           AS zip

FROM raw__addresses