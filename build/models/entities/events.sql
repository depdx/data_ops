WITH dedupe AS (

  SELECT

    * , 
    ROW_NUMBER() OVER(
      PARTITION BY case_code, title, date, 
                   time, creation_date
      ORDER BY _scrapy_timestamp DESC
    )

  FROM public.raw__events

)

SELECT

  case_code,
  title,
  date,
  time, 
  CASE
    WHEN canceled IS NULL THEN FALSE
    WHEN canceled ='CANCELED' THEN TRUE
  END as canceled,
  issued_date,
  creation_date,
  link,
  officer,
  result,
  status,
  status_date

FROM dedupe
WHERE row_number = 1
