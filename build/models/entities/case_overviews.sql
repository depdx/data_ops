WITH dedupe AS (

  SELECT

    * , 
    ROW_NUMBER() OVER(
      PARTITION BY case_code
      ORDER BY _scrapy_timestamp DESC
    )

  FROM public.raw__case_overviews

)

SELECT

  case_code,
  style,
  case_type,
  url,
  date, 
  location AS county,
  status
  
FROM dedupe
WHERE row_number = 1