WITH dedupe AS (

  SELECT

    * , 
    ROW_NUMBER() OVER(
      PARTITION BY case_code, case_type, date, 
                   decision, party
      ORDER BY _scrapy_timestamp DESC
    )

  FROM public.raw__judgements

)

SELECT

  case_code,
  case_type,
  date,
  decision,
  party

FROM dedupe
WHERE row_number = 1