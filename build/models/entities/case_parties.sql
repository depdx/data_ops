WITH dedupe AS (

  SELECT

    * , 
    ROW_NUMBER() OVER(
      PARTITION BY case_code, party_side,
                  name, addr
      ORDER BY _scrapy_timestamp DESC
    )

  FROM public.raw__case_parties

)

SELECT

  case_code,
  party_side,
  CASE
    WHEN reference IS NOT NULL
      THEN TRUE
    ELSE FALSE
  END                                  AS is_third_party,
  evictors.type                        AS third_party_type,
  dedupe.name,
  COALESCE(evictors.name, dedupe.name) AS entity_name,
  addr AS address,
  others,
  removed
FROM dedupe
LEFT JOIN public.evictors ON dedupe.name = evictors.reference
WHERE row_number = 1
