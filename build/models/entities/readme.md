# Entities

These models create a (somewhat) normalized relational schema of the data that we have available for this project.


``` mermaid
erDiagram

  CASE_OVERVIEWS {
    case_code text
    style text
    case_type text
    url text
    date timestamp
    county text
    status text
  }

  JUDGEMENTS {
    case_code text
    case_type text
    date timestamp
    decision text
    party text
  }

  LAWYERS{
    case_code text
    name text
    party_name text
    status text
    striked boolean
  }

  CASE_PARTIES {
    case_code text
    party_side text
    is_third_party boolean
    third_party_type text
    name text
    entity_name text
    address text
    others text
    removed timestamp
  }

  EVENTS {
    case_code text
    title text
    date timestamp
    time text
    canceled boolean
    issued_date timestamp
    creation_date timestamp
    link text
    officer text
    result text
    status text
    status_date text
  }

  CASE_OVERVIEWS ||--|{ EVENTS : case_code
  CASE_OVERVIEWS ||--|{ JUDGEMENTS : case_code
  CASE_OVERVIEWS ||--|{ LAWYERS : case_code
  CASE_OVERVIEWS ||--|{ CASE_PARTIES : case_code

```

