
WITH defendant_addresses AS (
  
  SELECT

    case_code,
    address,
    ROW_NUMBER() OVER(
      PARTITION BY case_code
      ORDER BY case_parties.address ASC
    )

  FROM {{ref('case_parties')}}
  WHERE lower(party_side) ='defendant'

), case_overviews AS (

  SELECT

    case_overviews.style,
    case_overviews.case_code,
    case_overviews.case_type,
    case_overviews.date,
    case_overviews.url,
    case_overviews.status,
    case_overviews.county         AS location,
    addresses.building_address    AS defendant_bldg_addr,
    addresses.city,
    addresses.directional         AS addr_directional,
    addresses.zip,
    regexp_replace(
      left(case_overviews.style, strpos(case_overviews.style, 'vs') - 1),
      E'[\\n\\r]+', ' ', 'g'
    )                             AS style_plaintiff

  FROM {{ref('case_overviews')}} AS case_overviews
  LEFT JOIN defendant_addresses ON case_overviews.case_code = defendant_addresses.case_code and ROW_NUMBER = 1
  LEFT JOIN {{ref('addresses')}} AS addresses ON defendant_addresses.address = addresses.address
  WHERE lower(county) IN ('multnomah', 'washington', 'clackamas')
    AND case_overviews.style IS NOT NULL

)

SELECT DISTINCT

  *, 
  split_part(style_plaintiff, ',', 1) AS style_plaintiff_entity,
  split_part(style_plaintiff, ',', 2) AS style_plaintiff_qualifier

FROM case_overviews