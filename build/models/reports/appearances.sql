
SELECT
  events.case_code,
  case_overviews.case_type,
  case_overviews.style,
  events.officer,
  concat(
    date(events.date),
    ' ',
    events.time)       AS when,
  events.date,
  events.time,
  events.title         AS hearing_type
FROM {{ref('events')}} AS events
INNER JOIN {{ref('case_overviews')}} AS case_overviews ON events.case_code = case_overviews.case_code
WHERE canceled = FALSE
  AND LOWER(case_overviews.county) = 'multnomah'
  AND title IN (
       'Hearing - Non Compliance', 'Call', 'Hearing - Landlord/Tenant',
       'Hearing - Motion', 'Appearance', 'Trial - Court',
       'Hearing - Further Proceedings', 'Hearing - Case Management',
       'Hearing', 'Hearing - Initial Appearance'
  )