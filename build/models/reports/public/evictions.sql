
WITH defendant_addresses AS (
  
  SELECT

    case_parties.case_code,
    case_parties.address,
    addresses.city,
    addresses.directional,
    addresses.street,
    addresses.steet_type,
    addresses.zip,
    ROW_NUMBER() OVER(
      PARTITION BY case_code
      ORDER BY case_parties.address ASC
    )

  FROM {{ref('case_parties')}} AS case_parties
  LEFT JOIN {{ref('addresses')}} AS addresses ON case_parties.address = addresses.address 
  WHERE LOWER(party_side) ='defendant'

), lawyers AS(

  SELECT
    case_code,
    ARRAY_AGG(name) AS evicting_lawyers
  FROM {{ref('lawyers')}} AS lawyers
  WHERE striked = FALSE
    AND LOWER(party_name) IN (
      SELECT LOWER(name)
      FROM {{ref('case_parties')}} AS case_parties
      WHERE LOWER(party_side) ='plaintiff'
    )
GROUP BY 1

), agents AS (

  SELECT
    case_code,
    ARRAY_AGG(entity_name) AS evicting_agents
  FROM {{ref('case_parties')}} AS case_parties
  WHERE party_side = 'Agent'
  GROUP BY 1

), landlords AS (

  SELECT
    case_code,
    ARRAY_AGG(LOWER(name)) AS evicting_landlords
  FROM {{ref('case_parties')}} AS case_parties
  WHERE LOWER(party_side) = 'plaintiff'
    AND is_third_party = FALSE
  GROUP BY 1

),property_managers AS (

  SELECT
    case_code,
    ARRAY_AGG(entity_name) AS evicting_property_managers
  FROM {{ref('case_parties')}} AS case_parties
  WHERE party_side = 'Plaintiff'
    AND third_party_type = 'property management'
  GROUP BY 1

), appearaces AS (

  SELECT
    case_code,
    title,
    date
  from {{ref('events')}} AS events
  WHERE canceled = FALSE
    AND title IN (
        'Hearing - Non Compliance', 'Call', 'Hearing - Landlord/Tenant',
        'Hearing - Motion', 'Appearance', 'Trial - Court',
        'Hearing - Further Proceedings', 'Hearing - Case Management',
        'Hearing', 'Hearing - Initial Appearance'
    )

), first_appearances AS (

  SELECT
    case_code,
    MIN(date) AS date
  FROM appearaces
  GROUP BY 1

), next_appearances AS (

  SELECT
    case_code,
    MIN(date) AS date
  FROM appearaces
  WHERE date >= CURRENT_DATE
  GROUP BY 1

), last_appearances AS (

  SELECT
    case_code,
    MAX(date) AS date
  FROM appearaces
  WHERE date < CURRENT_DATE
  GROUP BY 1

)

SELECT

  case_overviews.case_code,
  case_overviews.date                             AS filed_date,
  regexp_replace(
    case_overviews.style,
    '(.)(vs).*', ' vs tenant',
    'i'
    )                                             AS case_description,
  case_overviews.status,
  case_overviews.county,
  defendant_addresses.city,
  defendant_addresses.directional,
  -- CONCAT(
  --   defendant_addresses.street,
  --   ' ',
  --   defendant_addresses.steet_type
  --  )                            AS street,
  defendant_addresses.zip,
  property_managers.evicting_property_managers,
  landlords.evicting_landlords,
  lawyers.evicting_lawyers,
  agents.evicting_agents,
  first_appearances.date                          AS first_appearance_date,
  next_appearances.date                           AS next_appearance_date,
  last_appearances.date                           AS last_appearance_date

FROM {{ref('case_overviews')}} AS case_overviews
LEFT JOIN defendant_addresses ON case_overviews.case_code = defendant_addresses.case_code AND defendant_addresses.row_number = 1
LEFT JOIN lawyers on case_overviews.case_code = lawyers.case_code
LEFT JOIN agents on case_overviews.case_code = agents.case_code
LEFT JOIN landlords on case_overviews.case_code = landlords.case_code
LEFT JOIN property_managers on case_overviews.case_code = property_managers.case_code
LEFT JOIN first_appearances on case_overviews.case_code = first_appearances.case_code
LEFT JOIN next_appearances on case_overviews.case_code = next_appearances.case_code
LEFT JOIN last_appearances on case_overviews.case_code = last_appearances.case_code
WHERE lower(county) IN ('multnomah', 'washington', 'clackamas')
  AND case_type IN ('Residential or Return of Personal Property', 'Residential Eviction')
