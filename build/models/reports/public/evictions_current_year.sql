SELECT *
FROM {{ ref('evictions') }}
WHERE date_part('year', filed_date) = date_part('year', CURRENT_DATE)