# Seed Data for Evictions

These files are managed via this repository and are used within our data modeling and analysis of eviction data.

## evictors.csv

This is a maintained list of top evictors from different categories (landlords, property management, legal agents, and other real estate services) and allows us to solve some of our entity identification problems, such as psuednommous entities like Action Services as well as differenciating between landlords and real estate services like property management in most cases. 

schema:

| column    | description |
|-----------|-------------|
| name      | The name we use for the entity (deduplicated) |
| reference | The name used in the data source |
| type      | The type of entity, such as 'property management' |
