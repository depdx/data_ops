# depdx data ops

_As a organization focused on eviction defense via tenant power, having up-to-date data is useful in informing our actions and operations. Don't Evict PDX has a dedicated Data Committee that maintains this repository and runs the daily updates that use this code. You can contact us at data[dot]depdx[at]gmail.com_

## Scope and Structure

The scope of this project covers the following operations:

1. Loading and/or compiling this data from our [scraper](https://gitlab.com/depdx/ojd-eviction-scraping) or other sources in `/load`
1. Building data models and reports from loaded and/or compiled data in `/build`
1. Orchestrating these two operations in `/orchestrate`

This project also includes a `Makefile` for convenience and consistency in building the environments needed for these operations.

### `/load`

We are using [pandas](https://pandas.pydata.org/) to process data files from our scraper as well as write that compiled and processed data to CSVs. We do this by defining descrete table classes, with included metadata and processing operations.

### `/build`

We are using [dbt](https://github.com/dbt-labs) for define and model this data into reports and other data models. This relies on the creation and configuration of a postgres db which is included in the `make db` recipe recipe in the `Makefile`. This also includes the `/build/data` which contains CSVs that are loaded into the database, including the collaborative CSVs we maintain to identify and track top evictors. 

### `/orchestrate`

Additional code is needed in order to run these operations that doesn't fit neatly into either `/load` or `/build` such as copying eviction case PDFs from a shared drive into our own or handling date logic for running the scraper that is unique to this project.

## Usage

At the moment this project is used primarily in goolge colab notebooks that are available only to the data committee and other depdx members. In general we strive to minimize the code and complexity of those notebooks and, where possible and helpful, work to always consildate logic from those notebooks into various resources in this repository.

### loading data into pandas dataframes

At the moment there is a class for each table in `/load/tables.py` which we access as a module as in

```python
from data_ops.load.tables import ( 
    CaseOverview,
    CaseParty,
    Event,
    File,
    Judgment,
    Lawyer,
)

caseOverview_csv = f"CaseOverviewItem.csv"
co = CaseOverview()
caseOverviewDF = co.get_df(caseOverview_csv)

caseParty_csv = f"CasePartyItem.csv"
cp = CaseParty()
casePartyDF = cp.get_df(caseParty_csv)

```

Work is needed here to considate `tables.py` into something like a [factory](https://en.wikipedia.org/wiki/Factory_(object-oriented_programming)) to minimize code redundancy in both the classes and in their use.

### using the postgres database

the `make db` recipe is currently set up to run on linux (which is what's run in google colab) and requires the following environment variables

```
PG_USER
PG_PASSWORD
EVICTIONS_DB
```

These correspond to the environment variables referenced in `build/profile/profiles.yml`, which are also required to run dbt.

_additional work needs to be done in order to beter support other operating systems._

We are currently loading the data into the database using `load_tables` in `/load/tables.py` 

```python
db_url = f"postgresql://{pg_user}:{pg_password}@localhost:5432/{eviction_db}"

load_list = [
    (caseOverviewDF, "raw__case_overviews"),
    (casePartyDF, "raw__case_parties"),
    (eventDF, "raw__events"),
    (fileDF, "raw__files"),
    (judgementDF, "raw__judgements"),
    (lawyerDF, "raw__lawyers"),
    (addrDF, "raw__addresses"),
]

load_tables(db_url)
```

### running dbt

The entire dbt project (all data files and models) can be run using `make models`. This assumes that you have the environment variables set, as noted above, along with a running postgres database loaded with the tables defined in `/load/tables.py` and named as above in `load_list`.

dbt compiles to python via jinja templating but is used entirely with SQL and YAML, which makes it very human readable. Also it allows us to build a transparent data lineage.
