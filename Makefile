db:
	@echo "making postgres..."
	@sudo apt update
	@apt install postgresql postgresql-contrib
	@service postgresql start
	@sudo -u postgres psql -c "CREATE USER $(PG_USER) WITH SUPERUSER"
	@sudo -u postgres psql -U postgres -c "ALTER USER $(PG_USER) PASSWORD '$(PG_PASSWORD)';"
	@sudo -u postgres psql -U postgres -c 'DROP DATABASE IF EXISTS "$(EVICTIONS_DB)";'
	@sudo -u postgres psql -U postgres -c 'CREATE DATABASE "$(EVICTIONS_DB)";'
	@echo "installing dbt"
	@pip3 install dbt-postgres==1.5.4

models:
	@cd build && dbt seed --full-refresh --profiles-dir profile
	@cd build && dbt run --profiles-dir profile
	@cd build && dbt test --profiles-dir profile
