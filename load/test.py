import datatest as dt
from datatest import validate, accepted, Extra
from math import nan
import warnings

warnings.filterwarnings("ignore", message="subset and superset warning")


def test_case_overviews(df):
    with accepted(Extra(nan)):
        dt.validate.subset(
            df["status"],
            {
                "Closed",
                "Open",
                "Stayed",
                "Reinstated",
                "Appeal",
                "Bankruptcy Pending",
                "Arbitration",
            },
        )

def test_case_parties(df):
    with accepted(Extra(nan)):
        dt.validate.subset(
            df["party_side"],
            {
                "Plaintiff",
                "Defendant",
                "Agent",
            },
        )