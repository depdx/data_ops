import pandas as pd
import pandas_usaddress
from sqlalchemy import create_engine


def get_df(
    csv_path: str, schema: dict, remove_list: list = ["_scrapy_timestamp"]
) -> pd.DataFrame:

    df = pd.read_csv(csv_path, index_col=None, header=0, encoding="utf-8")
    df = df.replace({r'[^\x00-\x7F]+':''}, regex=True) # this might be too hasty
    df = df.astype(schema)
    columns = list(schema.keys())
    subset = [i for i in columns if i not in remove_list]
    df = df.filter(columns)
    df = df.sort_values(by=["_scrapy_timestamp"]).drop_duplicates(
        subset=subset, keep="last"
    )

    return df


class CaseOverview:
    def __init__(self):

        self.schema = {
            "case_code": "object",
            "case_type": "category",
            "date": "datetime64[ns]",
            "location": "category",
            "status": "object",
            "style": "object",
            "url": "object",
            "_scrapy_timestamp": "datetime64[ns]",
        }

    def get_df(self, csv_path):

        remove_list = ["_scrapy_timestamp", "status", "style"]
        caseOverviewDF = get_df(csv_path, self.schema, remove_list)

        return caseOverviewDF


class CaseParty:
    def __init__(self):

        self.schema = {
            "addr": "object",
            "case_code": "object",
            "name": "object",
            "others": "object",
            "party_side": "category",
            "removed": "datetime64[ns]",
            "_scrapy_timestamp": "datetime64[ns]",
        }

    def get_df(self, csv_path):

        remove_list = ["_scrapy_timestamp", "addr", "others", "removed"]
        casePartyDF = get_df(csv_path, self.schema, remove_list)

        return casePartyDF


class Event:
    def __init__(self):

        self.schema = {
            "canceled": "category",
            "case_code": "object",
            "creation_date": "datetime64[ns]",
            "date": "datetime64[ns]",
            "issued_date": "datetime64[ns]",
            "link": "object",
            "officer": "object",
            "result": "category",
            "signed_date": "datetime64[ns]",
            "status": "object",
            "status_date": "object",
            "time": "object",
            "title": "object",
            "_scrapy_timestamp": "datetime64[ns]",
        }

    def get_df(self, csv_path):

        remove_list = ["_scrapy_timestamp", "status", "officer", "result"]
        eventDF = get_df(csv_path, self.schema)

        return eventDF


class File:
    def __init__(self):

        self.schema = {
            "case_code": "object",
            "file_urls": "object",
            "_scrapy_timestamp": "datetime64[ns]",
        }

    def get_df(self, csv_path):

        CasePartyDF = get_df(csv_path, self.schema)

        return CasePartyDF


class Judgment:
    def __init__(self):

        self.schema = {
            "case_code": "object",
            "case_type": "object",
            "date": "datetime64[ns]",
            "decision": "object",
            "party": "object",
            "_scrapy_timestamp": "datetime64[ns]",
        }

    def get_df(self, csv_path):

        judgmentDF = get_df(csv_path, self.schema)

        return judgmentDF


class Lawyer:
    def __init__(self):

        self.schema = {
            "case_code": "object",
            "name": "object",
            "party_name": "object",
            "status": "category",
            "striked": "bool",
            "_scrapy_timestamp": "datetime64[ns]",
        }

    def get_df(self, csv_path):

        lawyerDF = get_df(csv_path, self.schema)

        return lawyerDF

def get_addrDF(casePartyDF: pd.DataFrame) -> pd.DataFrame:

    addrDF = casePartyDF.loc[:, ['addr']]

    addrDF = pandas_usaddress.tag(
      addrDF, ["addr"], granularity="high", standardize=False
      )

    unit_names = addrDF['OccupancyType'].dropna().unique()

    pat='('+'|'.join(unit_names)+'|#)\s?[^\s]+\s'

    addrDF["bldg_addr"] = addrDF["addr"].str.replace(pat, '', case=False, regex=True)
    addrDF["bldg_addr"] = addrDF["bldg_addr"].str.replace('\s+', ' ', case=False, regex=True)

    address_schema ={
        "addr": "object",
        "bldg_addr": "object",
        "AddressNumber": "object",
        "OccupancyType": "object",
        "OccupancyIdentifier": "object",
        "PlaceName": "object",
        "StateName": "object",
        "StreetName": "object",
        "StreetNamePreDirectional": "object",
        "StreetNamePostType": "object",
        "USPSBoxID": "object",
        "USPSBoxType": "object",
        "ZipCode": "object",
    }

    columns = list(address_schema.keys())
    addrDF = addrDF.filter(columns)
    addrDF = addrDF.astype(address_schema)

    return addrDF


def load_tables(db_url: str, load_list: list):

    engine = create_engine(db_url)

    for df, table_name in load_list:
        print(f"building table {table_name}...")
        df.to_sql(
            table_name, engine, if_exists="replace", index=False, index_label=False
        )
